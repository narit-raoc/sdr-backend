import sys
import os
import argparse
from argparse import RawTextHelpFormatter
import numpy as np
import matplotlib
matplotlib.use("QT5Agg")
import matplotlib.pyplot as plt
plt.style.use('dark_background')

helpstring = "TODO - write more documentation)"

# Setup rules for command line arguments
parser = argparse.ArgumentParser(description='Reads file from GNURadio and shows some graphs',
									epilog=helpstring, formatter_class=RawTextHelpFormatter)
parser.add_argument('filename_in', help='File written by GNURadio File Sink')

# If no command line arguments are avaiable, show help message and exit
if len(sys.argv[1:]) == 0:
	parser.print_help()
	exit()

# Read command line arguments into argparse structure
args = parser.parse_args()

if(os.path.isfile(args.filename_in) is not True):
	print('ERROR: File not found')
	print('os.path.isfile(%s) = False' % args.filename_in)
	exit()

# We have not exited, so process the file
print('Processing file %s: ' % args.filename_in)

# TODO -- read the header bytes, parse the header, then read the raw data.
# temporary - seek past the header and read binary data bytes. FIX ME!!
file_in = open(args.filename_in, 'rb')
file_in.seek(342)

# A stream item of type GNURadio "float" is represented in binary data as
# a continuous array of 32-bit (4-byte) floating point numbers.  

# If the GNURadio application reduces the complex-valued data to be an intensity
# or some other real-valued data, use this data type map.  
dtype_gr_real32 = np.dtype([('real',np.float32)])

# Read the raw bytes into a numpy structured array using the 
# map defined in dtype_gr_complex32
data = np.fromfile(file_in, dtype=dtype_gr_real32)

# Python / Numpy works natively with complex analytical signals, therefore
# we use python dictionary syntax to combine the separate real and imaginary components
# into Python complex floating point numbers that we can use with DSP functions 

# Close the file
file_in.close()

# Print stuff
print('read file: name = %s, count of real samples = %d' % (args.filename_in, data.shape[0]))


# Make plots
# Generate data points for the horizontal axis
figure_size = (11,7)
fig1 = plt.figure('analyze_cont', figsize=figure_size)

ax1 = plt.subplot2grid((1,1), (0,0), rowspan=1, colspan=1)
ax1.plot(data[7:-1],'+y', label='real')
ax1.grid(True, which='both', alpha=0.2)
#ax1.legend(loc=3)

plt.show()