# sdr-backend

Backend and digitizer configuration for radio astronomy.  Based on GNURadio and Ettus USRP software stack (RFNoC + UHD).  Plan to use with the USRP N310 SDR.