options:
  parameters:
    author: ssarris
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: ''
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: qt_gui
    hier_block_src_path: '.:'
    id: continuum
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: /home/ssarris/src/sdr-backend/dark.qss
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: prompt
    sizing_mode: fixed
    thread_safe_setters: ''
    title: Continuum Detector
    window_size: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 8]
    rotation: 0
    state: enabled

blocks:
- name: decimation1
  id: variable
  parameters:
    comment: ''
    value: 2**16
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [536, 16]
    rotation: 0
    state: true
- name: decimation2
  id: variable
  parameters:
    comment: ''
    value: 2**5
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [532, 86]
    rotation: 0
    state: true
- name: filename
  id: variable
  parameters:
    comment: ''
    value: datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S")+".f32"
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [325, 81]
    rotation: 0
    state: enabled
- name: filename_m
  id: variable
  parameters:
    comment: ''
    value: datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S")+".f32m"
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [326, 147]
    rotation: 0
    state: enabled
- name: rf_freq
  id: variable
  parameters:
    comment: ''
    value: 100e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [415, 12]
    rotation: 0
    state: true
- name: rx_gain
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: 0,1,1,1
    label: RX Gain (dB)
    min_len: '200'
    orient: Qt.Horizontal
    rangeType: float
    start: '0'
    step: '0.5'
    stop: '75'
    value: '30'
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1256, 95]
    rotation: 0
    state: true
- name: samp_rate
  id: variable
  parameters:
    comment: ''
    value: 20e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [325, 12]
    rotation: 0
    state: enabled
- name: analog_noise_source_x_0
  id: analog_noise_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: 1e-3
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    noise_type: analog.GR_GAUSSIAN
    seed: '0'
    type: complex
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [54, 295]
    rotation: 0
    state: disabled
- name: analog_sig_source_x_0
  id: analog_sig_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: 1e-3
    comment: ''
    freq: '0'
    maxoutbuf: '0'
    minoutbuf: '0'
    offset: '0'
    phase: '0'
    samp_rate: samp_rate
    type: complex
    waveform: analog.GR_COS_WAVE
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [42, 169]
    rotation: 0
    state: disabled
- name: blocks_add_xx_0
  id: blocks_add_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [332, 254]
    rotation: 0
    state: disabled
- name: blocks_complex_to_mag_squared_0
  id: blocks_complex_to_mag_squared
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [453, 398]
    rotation: 0
    state: true
- name: blocks_file_meta_sink_0
  id: blocks_file_meta_sink
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    detached: 'False'
    extra_dict: pmt.make_dict()
    file: filename_m
    max_seg_size: '1000000'
    rel_rate: 1/decimation1/decimation2
    samp_rate: '1'
    type: float
    unbuffered: 'False'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1254, 510]
    rotation: 0
    state: enabled
- name: blocks_file_sink_0
  id: blocks_file_sink
  parameters:
    affinity: ''
    alias: ''
    append: 'False'
    comment: ''
    file: filename
    type: float
    unbuffered: 'False'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1256, 420]
    rotation: 0
    state: enabled
- name: blocks_integrate_xx_0
  id: blocks_integrate_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decim: int(decimation1)
    maxoutbuf: '0'
    minoutbuf: '0'
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [675, 396]
    rotation: 0
    state: true
- name: blocks_integrate_xx_0_0
  id: blocks_integrate_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decim: int(decimation2)
    maxoutbuf: '0'
    minoutbuf: '0'
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [881, 395]
    rotation: 0
    state: true
- name: blocks_nlog10_ff_0
  id: blocks_nlog10_ff
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    k: -10*np.log10(decimation1*decimation2)
    maxoutbuf: '0'
    minoutbuf: '0'
    n: '10'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1063, 394]
    rotation: 0
    state: true
- name: blocks_null_sink_0
  id: blocks_null_sink
  parameters:
    affinity: ''
    alias: ''
    bus_structure_sink: '[[0,],]'
    comment: ''
    num_inputs: '1'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [279, 451]
    rotation: 0
    state: disabled
- name: blocks_null_source_0
  id: blocks_null_source
  parameters:
    affinity: ''
    alias: ''
    bus_structure_source: '[[0,],]'
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_outputs: '1'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [57, 379]
    rotation: 0
    state: disabled
- name: blocks_tag_debug_0
  id: blocks_tag_debug
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    display: 'True'
    filter: '""'
    name: ''
    num_inputs: '1'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [482, 578]
    rotation: 0
    state: true
- name: blocks_throttle_0
  id: blocks_throttle
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    ignoretag: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    samples_per_second: samp_rate
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [144, 411]
    rotation: 0
    state: disabled
- name: import_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import numpy as np
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [196, 11]
    rotation: 0
    state: true
- name: import_0_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import pmt
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [198, 119]
    rotation: 0
    state: true
- name: import_1
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import datetime
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [195, 69]
    rotation: 0
    state: true
- name: integration_sec
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: ''
    value: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1042, 7]
    rotation: 0
    state: true
- name: qtgui_freq_sink_x_0
  id: qtgui_freq_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    average: '1.0'
    axislabels: 'True'
    bw: samp_rate
    color1: '"blue"'
    color10: '"dark blue"'
    color2: '"red"'
    color3: '"green"'
    color4: '"black"'
    color5: '"cyan"'
    color6: '"magenta"'
    color7: '"yellow"'
    color8: '"dark red"'
    color9: '"dark green"'
    comment: ''
    ctrlpanel: 'False'
    fc: rf_freq
    fftsize: '1024'
    freqhalf: 'True'
    grid: 'False'
    gui_hint: 0,0,2,1
    label: Power (dB uncal)
    label1: ''
    label10: ''''''
    label2: ''''''
    label3: ''''''
    label4: ''''''
    label5: ''''''
    label6: ''''''
    label7: ''''''
    label8: ''''''
    label9: ''''''
    legend: 'False'
    maxoutbuf: '0'
    minoutbuf: '0'
    name: '""'
    nconnections: '1'
    showports: 'False'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_tag: '""'
    type: complex
    units: dB
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    wintype: firdes.WIN_BLACKMAN_hARRIS
    ymax: '10'
    ymin: '-140'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [677, 274]
    rotation: 0
    state: enabled
- name: qtgui_number_sink_0
  id: qtgui_number_sink
  parameters:
    affinity: ''
    alias: ''
    autoscale: 'False'
    avg: '0'
    color1: ("black", "white")
    color10: ("black", "black")
    color2: ("black", "black")
    color3: ("black", "black")
    color4: ("black", "black")
    color5: ("black", "black")
    color6: ("black", "black")
    color7: ("black", "black")
    color8: ("black", "black")
    color9: ("black", "black")
    comment: ''
    factor1: '1'
    factor10: '1'
    factor2: '1'
    factor3: '1'
    factor4: '1'
    factor5: '1'
    factor6: '1'
    factor7: '1'
    factor8: '1'
    factor9: '1'
    graph_type: qtgui.NUM_GRAPH_NONE
    gui_hint: 1,1,1,1
    label1: Power
    label10: ''
    label2: ''
    label3: ''
    label4: ''
    label5: ''
    label6: ''
    label7: ''
    label8: ''
    label9: ''
    max: '0'
    min: '-120'
    name: '"Results"'
    nconnections: '1'
    type: float
    unit1: dB uncal
    unit10: ''
    unit2: ''
    unit3: ''
    unit4: ''
    unit5: ''
    unit6: ''
    unit7: ''
    unit8: ''
    unit9: ''
    update_time: '0.10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1250, 228]
    rotation: 0
    state: true
- name: qtgui_time_sink_x_0
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'False'
    grid: 'False'
    gui_hint: 1,0,1,1
    label1: Signal 1
    label10: Signal 10
    label2: Signal 2
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '""'
    nconnections: '1'
    size: '60'
    srate: samp_rate/decimation1/decimation2
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: float
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Power (dB)
    ymax: '0'
    ymin: '-100'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1249, 329]
    rotation: 0
    state: disabled
- name: uhd_usrp_source_0
  id: uhd_usrp_source
  parameters:
    affinity: ''
    alias: ''
    ant0: RX2
    ant1: RX2
    ant10: RX2
    ant11: RX2
    ant12: RX2
    ant13: RX2
    ant14: RX2
    ant15: RX2
    ant16: RX2
    ant17: RX2
    ant18: RX2
    ant19: RX2
    ant2: RX2
    ant20: RX2
    ant21: RX2
    ant22: RX2
    ant23: RX2
    ant24: RX2
    ant25: RX2
    ant26: RX2
    ant27: RX2
    ant28: RX2
    ant29: RX2
    ant3: RX2
    ant30: RX2
    ant31: RX2
    ant4: RX2
    ant5: RX2
    ant6: RX2
    ant7: RX2
    ant8: RX2
    ant9: RX2
    bw0: '0'
    bw1: '0'
    bw10: '0'
    bw11: '0'
    bw12: '0'
    bw13: '0'
    bw14: '0'
    bw15: '0'
    bw16: '0'
    bw17: '0'
    bw18: '0'
    bw19: '0'
    bw2: '0'
    bw20: '0'
    bw21: '0'
    bw22: '0'
    bw23: '0'
    bw24: '0'
    bw25: '0'
    bw26: '0'
    bw27: '0'
    bw28: '0'
    bw29: '0'
    bw3: '0'
    bw30: '0'
    bw31: '0'
    bw4: '0'
    bw5: '0'
    bw6: '0'
    bw7: '0'
    bw8: '0'
    bw9: '0'
    center_freq0: 870e6
    center_freq1: '0'
    center_freq10: '0'
    center_freq11: '0'
    center_freq12: '0'
    center_freq13: '0'
    center_freq14: '0'
    center_freq15: '0'
    center_freq16: '0'
    center_freq17: '0'
    center_freq18: '0'
    center_freq19: '0'
    center_freq2: '0'
    center_freq20: '0'
    center_freq21: '0'
    center_freq22: '0'
    center_freq23: '0'
    center_freq24: '0'
    center_freq25: '0'
    center_freq26: '0'
    center_freq27: '0'
    center_freq28: '0'
    center_freq29: '0'
    center_freq3: '0'
    center_freq30: '0'
    center_freq31: '0'
    center_freq4: '0'
    center_freq5: '0'
    center_freq6: '0'
    center_freq7: '0'
    center_freq8: '0'
    center_freq9: '0'
    clock_rate: 0e0
    clock_source0: gpsdo
    clock_source1: ''
    clock_source2: ''
    clock_source3: ''
    clock_source4: ''
    clock_source5: ''
    clock_source6: ''
    clock_source7: ''
    comment: ''
    dc_offs_enb0: '""'
    dc_offs_enb1: '""'
    dc_offs_enb10: '""'
    dc_offs_enb11: '""'
    dc_offs_enb12: '""'
    dc_offs_enb13: '""'
    dc_offs_enb14: '""'
    dc_offs_enb15: '""'
    dc_offs_enb16: '""'
    dc_offs_enb17: '""'
    dc_offs_enb18: '""'
    dc_offs_enb19: '""'
    dc_offs_enb2: '""'
    dc_offs_enb20: '""'
    dc_offs_enb21: '""'
    dc_offs_enb22: '""'
    dc_offs_enb23: '""'
    dc_offs_enb24: '""'
    dc_offs_enb25: '""'
    dc_offs_enb26: '""'
    dc_offs_enb27: '""'
    dc_offs_enb28: '""'
    dc_offs_enb29: '""'
    dc_offs_enb3: '""'
    dc_offs_enb30: '""'
    dc_offs_enb31: '""'
    dc_offs_enb4: '""'
    dc_offs_enb5: '""'
    dc_offs_enb6: '""'
    dc_offs_enb7: '""'
    dc_offs_enb8: '""'
    dc_offs_enb9: '""'
    dev_addr: '""'
    dev_args: '""'
    gain0: rx_gain
    gain1: '0'
    gain10: '0'
    gain11: '0'
    gain12: '0'
    gain13: '0'
    gain14: '0'
    gain15: '0'
    gain16: '0'
    gain17: '0'
    gain18: '0'
    gain19: '0'
    gain2: '0'
    gain20: '0'
    gain21: '0'
    gain22: '0'
    gain23: '0'
    gain24: '0'
    gain25: '0'
    gain26: '0'
    gain27: '0'
    gain28: '0'
    gain29: '0'
    gain3: '0'
    gain30: '0'
    gain31: '0'
    gain4: '0'
    gain5: '0'
    gain6: '0'
    gain7: '0'
    gain8: '0'
    gain9: '0'
    iq_imbal_enb0: '""'
    iq_imbal_enb1: '""'
    iq_imbal_enb10: '""'
    iq_imbal_enb11: '""'
    iq_imbal_enb12: '""'
    iq_imbal_enb13: '""'
    iq_imbal_enb14: '""'
    iq_imbal_enb15: '""'
    iq_imbal_enb16: '""'
    iq_imbal_enb17: '""'
    iq_imbal_enb18: '""'
    iq_imbal_enb19: '""'
    iq_imbal_enb2: '""'
    iq_imbal_enb20: '""'
    iq_imbal_enb21: '""'
    iq_imbal_enb22: '""'
    iq_imbal_enb23: '""'
    iq_imbal_enb24: '""'
    iq_imbal_enb25: '""'
    iq_imbal_enb26: '""'
    iq_imbal_enb27: '""'
    iq_imbal_enb28: '""'
    iq_imbal_enb29: '""'
    iq_imbal_enb3: '""'
    iq_imbal_enb30: '""'
    iq_imbal_enb31: '""'
    iq_imbal_enb4: '""'
    iq_imbal_enb5: '""'
    iq_imbal_enb6: '""'
    iq_imbal_enb7: '""'
    iq_imbal_enb8: '""'
    iq_imbal_enb9: '""'
    lo_export0: 'False'
    lo_export1: 'False'
    lo_export10: 'False'
    lo_export11: 'False'
    lo_export12: 'False'
    lo_export13: 'False'
    lo_export14: 'False'
    lo_export15: 'False'
    lo_export16: 'False'
    lo_export17: 'False'
    lo_export18: 'False'
    lo_export19: 'False'
    lo_export2: 'False'
    lo_export20: 'False'
    lo_export21: 'False'
    lo_export22: 'False'
    lo_export23: 'False'
    lo_export24: 'False'
    lo_export25: 'False'
    lo_export26: 'False'
    lo_export27: 'False'
    lo_export28: 'False'
    lo_export29: 'False'
    lo_export3: 'False'
    lo_export30: 'False'
    lo_export31: 'False'
    lo_export4: 'False'
    lo_export5: 'False'
    lo_export6: 'False'
    lo_export7: 'False'
    lo_export8: 'False'
    lo_export9: 'False'
    lo_source0: internal
    lo_source1: internal
    lo_source10: internal
    lo_source11: internal
    lo_source12: internal
    lo_source13: internal
    lo_source14: internal
    lo_source15: internal
    lo_source16: internal
    lo_source17: internal
    lo_source18: internal
    lo_source19: internal
    lo_source2: internal
    lo_source20: internal
    lo_source21: internal
    lo_source22: internal
    lo_source23: internal
    lo_source24: internal
    lo_source25: internal
    lo_source26: internal
    lo_source27: internal
    lo_source28: internal
    lo_source29: internal
    lo_source3: internal
    lo_source30: internal
    lo_source31: internal
    lo_source4: internal
    lo_source5: internal
    lo_source6: internal
    lo_source7: internal
    lo_source8: internal
    lo_source9: internal
    maxoutbuf: '0'
    minoutbuf: '0'
    nchan: '1'
    norm_gain0: 'False'
    norm_gain1: 'False'
    norm_gain10: 'False'
    norm_gain11: 'False'
    norm_gain12: 'False'
    norm_gain13: 'False'
    norm_gain14: 'False'
    norm_gain15: 'False'
    norm_gain16: 'False'
    norm_gain17: 'False'
    norm_gain18: 'False'
    norm_gain19: 'False'
    norm_gain2: 'False'
    norm_gain20: 'False'
    norm_gain21: 'False'
    norm_gain22: 'False'
    norm_gain23: 'False'
    norm_gain24: 'False'
    norm_gain25: 'False'
    norm_gain26: 'False'
    norm_gain27: 'False'
    norm_gain28: 'False'
    norm_gain29: 'False'
    norm_gain3: 'False'
    norm_gain30: 'False'
    norm_gain31: 'False'
    norm_gain4: 'False'
    norm_gain5: 'False'
    norm_gain6: 'False'
    norm_gain7: 'False'
    norm_gain8: 'False'
    norm_gain9: 'False'
    num_mboards: '1'
    otw: ''
    rx_agc0: Default
    rx_agc1: Default
    rx_agc10: Default
    rx_agc11: Default
    rx_agc12: Default
    rx_agc13: Default
    rx_agc14: Default
    rx_agc15: Default
    rx_agc16: Default
    rx_agc17: Default
    rx_agc18: Default
    rx_agc19: Default
    rx_agc2: Default
    rx_agc20: Default
    rx_agc21: Default
    rx_agc22: Default
    rx_agc23: Default
    rx_agc24: Default
    rx_agc25: Default
    rx_agc26: Default
    rx_agc27: Default
    rx_agc28: Default
    rx_agc29: Default
    rx_agc3: Default
    rx_agc30: Default
    rx_agc31: Default
    rx_agc4: Default
    rx_agc5: Default
    rx_agc6: Default
    rx_agc7: Default
    rx_agc8: Default
    rx_agc9: Default
    samp_rate: samp_rate
    sd_spec0: ''
    sd_spec1: ''
    sd_spec2: ''
    sd_spec3: ''
    sd_spec4: ''
    sd_spec5: ''
    sd_spec6: ''
    sd_spec7: ''
    show_lo_controls: 'False'
    stream_args: ''
    stream_chans: '[]'
    sync: sync
    time_source0: gpsdo
    time_source1: ''
    time_source2: ''
    time_source3: ''
    time_source4: ''
    time_source5: ''
    time_source6: ''
    time_source7: ''
    type: fc32
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [62, 552]
    rotation: 0
    state: enabled

connections:
- [analog_noise_source_x_0, '0', blocks_add_xx_0, '1']
- [analog_sig_source_x_0, '0', blocks_add_xx_0, '0']
- [blocks_add_xx_0, '0', blocks_complex_to_mag_squared_0, '0']
- [blocks_add_xx_0, '0', qtgui_freq_sink_x_0, '0']
- [blocks_complex_to_mag_squared_0, '0', blocks_integrate_xx_0, '0']
- [blocks_integrate_xx_0, '0', blocks_integrate_xx_0_0, '0']
- [blocks_integrate_xx_0_0, '0', blocks_nlog10_ff_0, '0']
- [blocks_nlog10_ff_0, '0', blocks_file_meta_sink_0, '0']
- [blocks_nlog10_ff_0, '0', blocks_file_sink_0, '0']
- [blocks_nlog10_ff_0, '0', qtgui_number_sink_0, '0']
- [blocks_nlog10_ff_0, '0', qtgui_time_sink_x_0, '0']
- [blocks_null_source_0, '0', blocks_throttle_0, '0']
- [blocks_throttle_0, '0', blocks_null_sink_0, '0']
- [uhd_usrp_source_0, '0', blocks_complex_to_mag_squared_0, '0']
- [uhd_usrp_source_0, '0', blocks_tag_debug_0, '0']
- [uhd_usrp_source_0, '0', qtgui_freq_sink_x_0, '0']

metadata:
  file_format: 1
