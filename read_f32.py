import sys
import os
import argparse
from argparse import RawTextHelpFormatter
import numpy as np
import matplotlib
matplotlib.use("QT5Agg")
import matplotlib.pyplot as plt
plt.style.use('dark_background')

helpstring = "TODO - write more documentation)"

# Setup rules for command line arguments
parser = argparse.ArgumentParser(description='Reads file from GNURadio and shows some graphs',
									epilog=helpstring, formatter_class=RawTextHelpFormatter)
parser.add_argument('filename_in', help='File written by GNURadio File Sink')

# If no command line arguments are avaiable, show help message and exit
if len(sys.argv[1:]) == 0:
	parser.print_help()
	exit()

# Read command line arguments into argparse structure
args = parser.parse_args()

if(os.path.isfile(args.filename_in) is not True):
	print('ERROR: File not found')
	print('os.path.isfile(%s) = False' % args.filename_in)
	exit()

# We have not exited, so process the file
print('Processing file %s: ' % args.filename_in)

# Open the file 
file_in = open(args.filename_in, 'rb')

# Define the Numpy data type that will be used to translate raw bytes into 
# dictionary fields of a Numpy structured array.
#
# We assume that the GNURadio data stream is operating with items of "complex"
# data type (blue color ports in GNURadio companion).  More specifically, 
# GNURadio "complex" type represents the complex analytical signal obtained by
# sampling 2 channels (I and Q) of a direct-downconversion receiver at 
# IF frequency 0 Hz.
#
# A stream item of type GNURadio "complex" is represented in binary data as
# two 32-bit (4-byte) floating point numbers.  The first float32 is the
# real component of the complex sample.  The second float32 is the imaginary  
dtype_gr_complex32 = np.dtype([('real',np.float32),('imag',np.float32)])

# If the GNURadio application reduces the complex-valued data to be an intensity
# or some other real-valued data, use this data type map.  
dtype_gr_real32 = np.dtype([('real',np.float32)])

# Read the raw bytes into a numpy structured array using the 
# map defined in dtype_gr_complex32
data_raw = np.fromfile(file_in, dtype=dtype_gr_complex32)

# Python / Numpy works natively with complex analytical signals, therefore
# we use python dictionary syntax to combine the separate real and imaginary components
# into Python complex floating point numbers that we can use with DSP functions 
sig = data_raw['real']+1j*data_raw['imag']

# Close the file
file_in.close()

# Print stuff
print('read file: name = %s, count of complex samples = %d' % (args.filename_in, sig.shape[0]))


# Make plots
# Generate data points for the horizontal axis
xx = np.arange(0,sig.shape[0])
figure_size = (11,7)
fig1 = plt.figure('analyze_iq', figsize=figure_size)

ax1 = plt.subplot2grid((1,1), (0,0), rowspan=1, colspan=1)
ax1.plot(xx, np.real(sig),'b--')
ax1.plot(xx, np.real(sig),'bo', label='real')
ax1.plot(xx, np.imag(sig),'r--')
ax1.plot(xx, np.imag(sig),'ro', label='imag')
ax1.set_title('Complex-Valued Signal')
ax1.set_xlabel('Sample Number [integer]')
ax1.set_ylabel('Amplitude [no unit]')
ax1.grid(True, which='both')
ax1.legend(loc=3)

plt.show()